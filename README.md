# Data Mining
This repo contains assignments and lab sessions delivered during the course of Data Mining held at Stockholm University (DSV dept.) on September/October 2017.

### Teaching
  - Panagiotis Papapetrou (professor)
  - Ram Bahadur Gurung (teaching assistant)
 
### Info
More info about the course can be found [here](https://daisy.dsv.su.se/servlet/Momentinfo?id=6295)
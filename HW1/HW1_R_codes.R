
# *********************************************
# DAMI Preprocessing Exercise R file
# Complete the codes to complete the assignment
# *********************************************
#
# Name: Giacomo Bartoli
# Student ID: 930130T538
#
# 1. Import data for analysis to R environment
# Downloaded "Adult" dataset from UCI Machine Learning Repository
# URL http://archive.ics.uci.edu/ml/datasets/Adult
# Import dataset in adult_db
# Missing values are represented as "?" in data file, make sure that R read them as missing values (NAs)
# HINT: use read.table() function, use ?read.table for more help
# ------------------------------------------------------------------------------------------------------ #
adult_db <- read.table(file = "http://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data",header = FALSE, sep = ",", na.strings = "?", strip.white = TRUE, stringsAsFactors = FALSE)
  
  
# Assign attribute names (column names) to the data we just imported
# Attribute names are in separate file "adult.names", scroll down to the bottom of this file
# Attribute names such as ("age", "workclass", "fnlwgt",...)
# Last column of the dataset adult.db with values ">50K" and "<=50K" does not have name, 
# this is the class attribute, so we just name it as "class"
# ----------------------------------------------------------------------------------------- #
names(adult_db) = c("age",
                    "workclass",
                    "fnlwgt",
                    "education",
                    "education_num",
                    "marital_status",
                    "occupation",
                    "relationship",
                    "race",
                    "sex",
                    "capital_gain",
                    "capital_loss",
                    "hours_per_week",
                    "native_country",
                    "class")

  
  
# Inspect data set in tabular form
# -----------------------------
fix(adult_db)

# Change class labels to 1 (adults who earn more than 50K) and 0 (adults who earn less than or equal to 50K)
# ----------------------------------------------------------------------------------------------
adult_db$class[adult_db$class==">50K"] <- 1
adult_db$class[adult_db$class=="<=50K"] <- 0


# 2. Check for missing values
# Write code to check how many missing values each attribute has
# Hint: use "apply()" function along columns of "adult.db", for each column (attribute) find how many NAs are there
# is.na(x) function can be used to see if x has NA, ?is.na for help
# --------------------------------------------------------------------------------------------------------------- #
apply(adult_db, 2, function(x) {sum(is.na(x))})

  
# Delete records (rows) with any missing value
# --------------------------------------- #
adult_db_nomiss <- na.omit(adult_db)
  
  
# 3. We will take only small chunk of the data for our experimental purpose.
# So, randomly select 1000 records from among 30 thousand records in the dataset.
# ------------------------------------------------------------------------------- #
set.seed(1013)
idx = sample(1:nrow(adult_db_nomiss),1000)
adult_db_lim = adult_db_nomiss[idx,]
row.names(adult_db_lim) <- NULL
fix(adult_db_lim)
  
  
  
# Examine attributes of the dataset
# 3a. Plot histogram for numeric attribute "age", with 50 breaks, show main title and attribute name on the plot.
# HINT: use hist() function for plotting histogram, ?hist to see how to use it.
# --------------------------------------------------------------------------------------------------------
red_age_idx <- (adult_db_lim$class == 0)
hist(adult_db_lim$age[red_age_idx], breaks = 50, xlab = "age", ylab = "frequency", col = "red", main = "Age in adults")
blue_age_idx <- (adult_db_lim$class == 1)
hist(adult_db_lim$age[blue_age_idx], breaks = 50, xlab = "age", ylab = "frequency", col = "blue", add=T)
legend(x=70, y=15, legend = c(">50K","<=50K"),
       col=c("blue", "red"), pch = 15, cex = 0.75)

# 3b. Plot barchart for categorical attribute "race", show legend, attribute name and main title for the plot.
# HINT: use barplot() function for plotting barchars, ?barplot for more help.
# --------------------------------------------------------------------------------------
height_of_bar <- table(adult_db_lim$race)
namesVector <-levels(adult_db_lim$race)
barplot(height_of_bar, col=c("black", "red", "green", "blue","cyan"),
        main = "Race of adults", 
        names.arg = namesVector,
        cex.names = 0.8)

labelsName=c("Amer-Indian-Eskimo","Asian-Pac-Islander","Black","Other","White")
legend(x=1, y=600, legend = labelsName,
       col=c("black", "red", "green", "blue","cyan"), pch = 15, cex = 0.75)
  
# 3c. Plot a boxplot for attribute "Age" and show possible outlier for this attribute
# HINT: ?boxplot for more help
# ---------------------------------------------------------------------------------------------
boxplot(adult_db_lim$age, pch=20, col="red", main = "Age of adults")
boxplot.stats(adult_db_lim$age)$out



#4 Create new data set from our latest dataset with only numeric attributes
# ------------------------------------------------------------------------
adult_db_numeric <- adult_db_lim[,c("age", "fnlwgt", "education_num", "capital_gain", "capital_loss", "hours_per_week")]
class_val <- as.numeric(adult_db_lim[,c("class")])



# Standardize numeric attributes in "adult_db_numeric" dataset.
# mean = 0 and sd = 1 for all numeric attributes
# -----------------------------------------------------------------------------------------------

adult_db_num_std <- scale(adult_db_numeric)
mean(adult_db_num_std)
sd(adult_db_num_std)
  
# we can check the mean and standard deviation of the standardized data
# ------------------------------------------------------------------
apply(adult_db_num_std, 2, mean)
apply(adult_db_num_std, 2, sd)
  
  
# 5a. Run Principal Component Analysis (PCA) on the numeric dataset from above "adult_db_num_std"
# plot the first 2 principal components
# HINT: for class specific colours, in plot(...) command use parameter col = (class_val + 2)
# HINT: ?prcomp to know about the parameters
# ------------------------------------------------------------------------------------------

# ******** YOUR CODE FOR GETTING PRINCIPAL COMPONENTS GOES HERE ******** #
pr.out <- prcomp(adult_db_num_std[,c("age", "fnlwgt", "education_num", "capital_gain", "capital_loss", "hours_per_week")], scale = TRUE, center = TRUE)
names(pr.out)
head(pr.out$x)
principal_components <- pr.out$x

# ******** PLOT FOR FIRST TWO PRINCIPAL COMPONENTS GOES HERE ****** #  
plot(principal_components[,1:2], col = (class_val + 2), pch = 20, main = "First two principal component")
legend(x=-6, y=5, legend = c("<=50K",">50K"),
       col=c("red", "green"), pch = 20, cex = 0.75)


# 5b. Plot percentage of the variance explained by principal components
# ----------------------------------------------------------------------------
# write a code to show proportion of variance explained by each Principal Component
# Standard deviation are stored as "sdev"

# *** YOUR CODE TO FIND PROPORTION OF VARIANCE EXPLAINED *** #
pr.var <- (pr.out$sdev)^2
pve <- pr.var/sum(pr.var)


# *** PLOT VARIANCE EXPLAINED BY PRINCIPAL COMPONENTS AND CUMULATIVE PROPORTION OF VARIANCE *** #
# par(...) for a plot with 2 figures.

par(mfrow=c(1,2), oma=c(0,0,2,0))
plot(pve, xlab = "PC", ylab = "Variance explained", type = "b", ylim = c(0,1))
plot(cumsum(pve), xlab = "PC", ylab = "Cumulative variance", type = "b", ylim = c(0,1))
mtext("Proportion of Variance explained by PC", outer = TRUE, cex=1.2)

  

# 5c. Write as comments how many principal components would you use to capture at least 50% variance
# and 90% varience respectively.

# ANSWER:
# The previous graph clearly shows that if want to capture 50% of variance we need at least 3 principal components,
# while if we want 90% of variance we need at least 5 components.
# In order to be more specific, we can print 'comsum(pve)' command on the R command line and see that:
# [1] 0.2289799 0.4037900 0.5654989 0.7243586 0.8731598 1.0000000
# So, using 3 principal components we got 0.56 while using 5 principal components we got 0.88.

# All the code can be found at the following repository: https://bitbucket.org/bgiacomo/dami1617/

            